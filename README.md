# HackTheBoxProgress

## Description

Progression des modules de l'académie HackTheBox

<object data="./HTB_Academy_Student_Transcript.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="./HTB_Academy_Student_Transcript.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="./HTB_Academy_Student_Transcript.pdf">Download PDF</a>.</p>
    </embed>
</object>